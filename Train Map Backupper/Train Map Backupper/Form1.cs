﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using UsefulTools;
using WPFFolderBrowser;

namespace Train_Map_Backupper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Source.Text != "" && BackupFolder.Text != "")
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\TrainMapFolder.txt",Source.Text + "\n" + BackupFolder.Text);
                SelectLocationPanel.Visible = false;
            }
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            await StartBackup();
        }

        public async Task StartBackup()
        {
            BackupButton.Visible = false;
            if (!File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\TrainMapFolder.txt"))
            {
                SelectLocationPanel.Visible = true;
            }
            else
            {
                SelectLocationPanel.Visible = false;
                string[] GetDetails = File.ReadAllLines(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\TrainMapFolder.txt");
                string BackupFolder = GetDetails[1];
                string SavesFolder = GetDetails[0] + "\\saves\\Train Map";
                Directory.CreateDirectory(BackupFolder + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year);
                string BackupDirectory = BackupFolder + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year;
                var i = 1;
                while (true)
                {
                    if (File.Exists(BackupDirectory + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "-0" + i + ".zip"))
                    {
                        i++;
                    }
                    else
                    {
                        BackupDirectory = BackupDirectory + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "-0" + i + ".zip";
                        break;
                    }
                }
                await Task.Factory.StartNew(() =>
                {
                    ZipFile.CreateFromDirectory(SavesFolder, BackupDirectory);
                });
            }
            BackupButton.Visible = true;
            if (checkBox1.Checked == true)
            {
                await Command.RunCommandHidden("shutdown.exe /s /f /t 0");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SelectLocationPanel.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                WPFFolderBrowserDialog d = new WPFFolderBrowserDialog();
                d.ShowDialog();
                Source.Text = d.FileName;
            }
            catch (Exception exception)
            {
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                WPFFolderBrowserDialog d = new WPFFolderBrowserDialog();
                d.ShowDialog();
                BackupFolder.Text = d.FileName;
            }
            catch (Exception exception)
            {

            }
        }
    }
}
