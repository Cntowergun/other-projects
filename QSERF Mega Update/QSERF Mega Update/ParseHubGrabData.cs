﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParseHub.Client;
using ParseHub.Client.Models;

namespace QSERF_Mega_Update
{
    public static class ParseHubGrabData
    {
        private static ParseHub.Client.ProjectRunner projectRunner = new ProjectRunner("tsec8884UacR");
        private static Project project = new Project();
        public static async Task<string> GetTrelloData()
        {
            var RunToken = projectRunner.RunProject("tbHJJ3MQRm2k").RunToken;
            while (!await RunDone(RunToken))
            {
                await Task.Delay(10);
            }
            return new ProjectRunner("tsec8884UacR").GetRun(RunToken).Data;
        }

        private static async Task<bool> RunDone(string RunID)
        {
            return new ProjectRunner("tsec8884UacR").GetRun(RunID).DataReady;
        }
    }
}
