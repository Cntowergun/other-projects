﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ParseHub.Client;
using ParseHub.Client.Models;

namespace QSERF_Update_Data_Grabber
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private static ParseHub.Client.ProjectRunner projectRunner = new ProjectRunner("tsec8884UacR");
        private static Project project = new Project();
        public static async Task<string> GetTrelloData()
        {
            var RunToken = projectRunner.RunProject("tbHJJ3MQRm2k").RunToken;
            BackgroundWorker d = new BackgroundWorker();
            
                while (!await RunDone(RunToken))
                {
                    await Task.Delay(10);
                }
            
            while (d.IsBusy)
            {
                await Task.Delay(10);
            }
            return new ProjectRunner("tsec8884UacR").GetRun(RunToken).Data;
        }
        private static string MainDirectory = "C:\\Users\\Administrator\\Desktop\\QSERF";
        private static async Task<bool> RunDone(string RunID)
        {
            return new ProjectRunner("tsec8884UacR").GetRun(RunID).DataReady;
        }
        private async void Form1_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            while (true)
            {
                Console.WriteLine("Getting Data...   " + DateTime.Now.ToString("F"));
                richTextBox1.Text += "\n" + "Getting Data...   " + DateTime.Now.ToString("F");
                string Data = await GetTrelloData();
                Console.WriteLine("Updating Data...   " + DateTime.Now.ToString("F"));
                richTextBox1.Text += "\n" + "Updating Data...   " + DateTime.Now.ToString("F");
                File.WriteAllText(MainDirectory + "\\updatedata.txt", Data);
                richTextBox1.Text += "\n" + "Cooling down...   " + DateTime.Now.ToString("F");
                Console.WriteLine("Cooling down...   " + DateTime.Now.ToString("F"));
                await Task.Delay(TimeSpan.FromMinutes(10));
            }
        }
    }
}
